﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Bookatable.Core.EEMS.Helpers
{
    public class JsonEnumConverter<T> : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var enumCellType = (T)value;
            foreach (T enumValue in Enum.GetValues(typeof(T)))
            {
                if (EqualityComparer<T>.Default.Equals(enumCellType, enumValue))
                {
                    string name = Enum.GetName(typeof(T),enumCellType); 
                    writer.WriteValue(name);
                }
            }
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var enumString = (string)reader.Value;

            return Enum.Parse(typeof(T), enumString, true);
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(string);
        }
    }
}
