﻿using Newtonsoft.Json;

namespace Bookatable.Core.EEMS.Domain
{
    [Newtonsoft.Json.JsonObject(Title = "request")]
    public class EEMSRequest
    {
        public EEMSRequest()
        {
            Header = new EEMSRequestHeader();
            Data = new EEMSRowEmailAddress();
        }
        [JsonProperty("header")]
        public EEMSRequestHeader Header { get; set; }

        [JsonProperty("data")]
        public EEMSRowEmailAddress Data { get; set; }
    }
}
