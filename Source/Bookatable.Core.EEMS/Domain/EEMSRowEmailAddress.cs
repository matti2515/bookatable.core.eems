﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Bookatable.Core.EEMS.Domain
{
    public class EEMSRowEmailAddress
    {
        public EEMSRowEmailAddress()
        {
            Row = new List<EEMSRequestRow>();
        }
        [JsonProperty("row")]
        public List<EEMSRequestRow> Row { get; set; }
    }
}
