﻿using Microsoft.Build.Framework;

namespace Bookatable.Core.EEMS.Domain
{
    public class EmailSignupUser
    {
        public string Qtype { get; set; }

        [Required]
        public string Email { get; set; }

        public int Location { get; set; }

        [Required]
        public string Culture { get; set; }

        [Required]
        public string Country { get; set; }

        public string TrackerRid { get; set; }

        public override string ToString()
        {
            return string.Format("{{Qtype: \"{0}\", Email:\"{1}\", Location: \"{2}\", Culture: \"{3}\", Country: \"{4}\", TrackerRid: \"{5}\"}}",
                Qtype, Email, Location, Culture, Country, TrackerRid);
        }
    }
}
