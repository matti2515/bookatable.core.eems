﻿using Bookatable.Core.EEMS.Domain.Enums;
using Bookatable.Core.EEMS.Helpers;
using Newtonsoft.Json;

namespace Bookatable.Core.EEMS.Domain
{
    public class EEMSRequestHeader
    {
        public EEMSRequestHeader()
        {
            ClientId = System.Configuration.ConfigurationManager.AppSettings["EEMSAPIClientId"];
            Password = System.Configuration.ConfigurationManager.AppSettings["EEMSAPIPassword"];
            NotificationAddress = System.Configuration.ConfigurationManager.AppSettings["EEMSAPINotificationAddress"];
            Version = "1.0";
            Source = "BOOKATABLE";
        }

        [JsonProperty("version")]
        public string Version;

        [JsonProperty("client_id")]
        public string ClientId;

        [JsonProperty("password")]
        public string Password;

        [JsonProperty("cell")]
        [JsonConverter(typeof(JsonEnumConverter<EEMSCellType>))]
        public EEMSCellType Cell;

        [JsonProperty("target")]
        [JsonConverter(typeof(JsonEnumConverter<EEMSTargetType>))]
        public EEMSTargetType Target;

        [JsonProperty("source")]
        public string Source;

        [JsonProperty("notification_address")]
        public string NotificationAddress;
    }
}
