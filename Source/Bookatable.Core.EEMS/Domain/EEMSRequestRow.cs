﻿using System;
using Bookatable.Core.EEMS.Domain.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Bookatable.Core.EEMS.Domain
{
    public class EEMSRequestRow
    {
        [JsonConstructor]
        public EEMSRequestRow()
        {

        }

        public EEMSRequestRow(EmailSignupUser model)
        {
            this.CustomerKey = -1;
            this.ConsumerType = EEMSConsumerType.B2C;
            this.Email = model.Email;
            this.Culture = model.Culture;
            this.Country = model.Country;
            this.Market = model.Country;
            this.LocationId = model.Location;
            this._dinningdate = DateTime.Now;
            this._unsubFlag = false;
        }
        
        [JsonProperty("EMAIL")]
        public string Email;

        [JsonProperty("UNSUB_FLAG")]
        public string UnsubFlagString
        {
            get { return _unsubFlag ? "Y" : "N"; }
            set { throw new ArgumentException("Value cannot be setted"); }
        }

        [JsonProperty("CONSUMERTYPE"), JsonConverter(typeof(StringEnumConverter))]
        public EEMSConsumerType ConsumerType;

        [JsonProperty("LOCATIONID")]
        public int LocationId;

        [JsonProperty("CUSTOMERKEY")]
        public int CustomerKey;

        [JsonProperty("CULTURE")]
        public string Culture;

        [JsonProperty("SALESFORCEID")]
        public int SalesForceId;

        [JsonProperty("DININGDATE")]
        public string DinningDate
        {
            get { return _dinningdate.ToString("yyyy-MM-dd HH:mm:ss"); }
            set { _dinningdate = DateTime.Parse(value); }
        }

        [JsonProperty("COUNTRY")]
        public string Country
        {
            get { return _country; }
            set { SetCountryName(value); }
        }
        
        [JsonProperty("MARKET")]
        public string Market
        {
            get { return _country; }

            set { SetCountryName(value); }
        }

        [JsonIgnore]
        private DateTime _dinningdate;

        [JsonIgnore]
        private string _country;

        [JsonIgnore]
        private bool _unsubFlag;

        private void SetCountryName(string countryName)
        {
            switch (countryName)
            {
                case "uk":
                    _country = "GBR";
                    break;
                case "sv":
                    _country = "SV";
                    break;
            }
        }
    }
}
