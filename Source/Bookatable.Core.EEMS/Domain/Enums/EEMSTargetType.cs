﻿namespace Bookatable.Core.EEMS.Domain.Enums
{
    public enum EEMSTargetType
    {
        ProfileUpdate,
        Autoresponder
    }
}
