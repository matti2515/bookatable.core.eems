﻿namespace Bookatable.Core.EEMS.Domain.Enums
{
    public enum EEMSConsumerType
    {
        B2B,
        B2C
    }
}
