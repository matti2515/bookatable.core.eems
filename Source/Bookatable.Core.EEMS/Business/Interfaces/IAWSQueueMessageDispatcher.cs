﻿using System.Net;

namespace Bookatable.Core.EEMS.Business.Interfaces
{
    public interface IAWSQueueMessageDispatcher
    {
        HttpStatusCode SendMessage(string messageBody);
    }
}
