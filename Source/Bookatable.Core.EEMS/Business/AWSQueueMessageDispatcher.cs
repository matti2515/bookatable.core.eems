﻿using System.Configuration;
using System.Net;
using Amazon.SQS;
using Amazon.SQS.Model;
using Bookatable.Core.EEMS.Business.Interfaces;

namespace Bookatable.Core.EEMS.Business
{
    public class AWSQueueMessageDispatcher : IAWSQueueMessageDispatcher
    {
        private AmazonSQSClient _amazonSqsClient;
        private GetQueueUrlResponse _queueUrl;

        public AWSQueueMessageDispatcher()
        {
            string accessKey = ConfigurationManager.AppSettings["AwsAccessKey"];
            string secretKey = ConfigurationManager.AppSettings["AwsSecretKey"];
            string name = ConfigurationManager.AppSettings["AwsNewsletterQueueName"];

            _amazonSqsClient = new AmazonSQSClient(accessKey, secretKey, Amazon.RegionEndpoint.EUWest1);
            _queueUrl = _amazonSqsClient.GetQueueUrl(name);
        }

        public HttpStatusCode SendMessage(string messageBody)
        {
            var messageRequest = new SendMessageRequest(_queueUrl.QueueUrl, messageBody);
            var messageResponse = _amazonSqsClient.SendMessage(messageRequest);

            return messageResponse.HttpStatusCode;
        }
    }
}
